// isValidate
var isValidated;
function isValidate(data) {
  isValidated = true;
  kiemTraRong(data.name, "spanTenSP", "* Tên sản phẩm không được rỗng");
  kiemTraRong(data.price, "spanGiaSP", "* Giá sản phẩm không được rỗng");
  kiemTraRong(
    data.linkimg,
    "spanHinhSP",
    "* Đường dẫn hình ảnh sản phẩm không được rỗng"
  );
  kiemTraRong(data.desc, "spanMoTa", "* Mô tả sản phẩm không được rỗng");
  kiemTraSo(data.price, "spanGiaSP", "* Giá sản phẩm phải là một số");
  kiemTraLinkIMG(
    data.linkimg,
    "spanHinhSP",
    "* Đường dẫn hình ảnh sản phẩm không đúng định dạng"
  );

  return isValidated;
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    isValidated = false;
  } else {
    hideMessage(idErr);
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d+$/;

  let isNumber = reg.test(value);
  if (isNumber) {
    hideMessage(idErr);
  } else {
    showMessageErr(idErr, message);
    isValidated = false;
  }
}

/* function kiemTraChu(value, idErr, message) {
  var reg =
    /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
  let isWords = reg.test(value);
  if (isWords) {
    hideMessage(idErr);
  } else {
    showMessageErr(idErr, message);
    isValidated = false;
  }
} */
function kiemTraLinkIMG(userInput, idErr, message) {
  var reg =
    /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g;
  let isWords = reg.test(userInput);
  if (isWords) {
    hideMessage(idErr);
  } else {
    showMessageErr(idErr, message);
    isValidated = false;
  }
}
