const BASE_URL_ADMIN = "https://637cf3cf72f3ce38eab27388.mockapi.io";
let error = "Hệ thống đang sử lí. Vui lòng thực hiện lại sau!";

// Goi Render lan dau
function fetchAllAdmin() {
  axios({
    url: `${BASE_URL_ADMIN}/product`,
    method: "GET",
  })
    .then(function (res) {
      renderProductList(res.data);
    })
    .catch(function () {
      alert(error);
    });
}

fetchAllAdmin();

// Remove san pham

function removeItem(id) {
  axios({
    url: `${BASE_URL_ADMIN}/product/${id}`,
    method: "DELETE",
  })
    .then(function () {
      fetchAllAdmin();
    })
    .catch(function () {
      alert(error);
    });
}

//  Add Product

function addProducts() {
  getInfo();
  var data = getInfo();
  if (isValidate(data)) {
    var newProduct = {
      name: data.name,
      price: data.price,
      linkimg: data.linkimg,
      desc: data.desc,
    };
    axios({
      url: `${BASE_URL_ADMIN}/product`,
      method: "POST",
      data: newProduct,
    })
      .then(function () {
        fetchAllAdmin();
        resetForm();
      })
      .catch(function () {
        alert(error);
      });
  }
}

// Edit san pham

function editItem(id) {
  displayBtn1.add("d-none");
  displayBtn2.remove("d-none");
  hideMessage("spanTenSP");
  hideMessage("spanGiaSP");
  hideMessage("spanHinhSP");
  hideMessage("spanMoTa");
  axios({
    url: `${BASE_URL_ADMIN}/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      document.getElementById("TenSP").value = res.data.name;
      document.getElementById("GiaSP").value = res.data.price;
      document.getElementById("HinhSP").value = res.data.linkimg;
      document.getElementById("MoTa").value = res.data.desc;
      idEditer = res.data.id;
    })
    .catch(function () {
      alert(error);
    });
}

// Update sản phẩm

function updateProduct() {
  let data = getInfo();
  if (isValidate(data)) {
    axios({
      url: `${BASE_URL_ADMIN}/product/${idEditer}`,
      method: "PUT",
      data: data,
    })
      .then(function () {
        fetchAllAdmin();
        resetForm();
      })
      .catch(function () {
        alert(error);
      });
  }
}
