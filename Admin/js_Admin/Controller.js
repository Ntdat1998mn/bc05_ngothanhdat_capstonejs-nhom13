// Render list sản phẩm
function renderProductList(list) {
  var no = 0;
  var contentHTML = list
    .map(function (item) {
      no++;
      return `
            <tr id="${item.id}">
                <td class="a text-center">${no}</td>
                <td>${item.name}</td>
                <td class="a text-center">${item.price}</td>
                <td>${item.linkimg}</td>
                <td>${item.desc}</td>
                <td class="a text-center">
                <button class="btn btn-danger" onclick="removeItem(${item.id})">
                  Xóa
                </button>
                <button class="btn btn-info" data-toggle="modal"
                data-target="#myModal" onclick="editItem(${item.id})">
                  Sửa
                </button>
              </td>
            </tr>
        `;
    })
    .join("");
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  document.querySelector("#myModal .close").click();
}

// Lấy thông tin sản phẩm nhập vào
function getInfo() {
  var name = document.getElementById("TenSP").value;
  var price = document.getElementById("GiaSP").value;
  var linkImg = document.getElementById("HinhSP").value;
  var desc = document.getElementById("MoTa").value;

  return {
    name: name,
    price: price,
    linkimg: linkImg,
    desc: desc,
  };
}

// Reset form
function resetForm() {
  document.getElementById("formSP").reset();
}

// Show messages
function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}

// Hide messages
function hideMessage(idErr) {
  document.getElementById(idErr).innerHTML = "";
}
// Ẩn hiện Element HTML
var displayBtn1 = document.querySelector(
  ".modal-footer button:nth-child(1)"
).classList;
var displayBtn2 = document.querySelector(
  ".modal-footer button:nth-child(2)"
).classList;
document.getElementById("btnThemSP").onclick = function () {
  hideMessage("spanTenSP");
  hideMessage("spanGiaSP");
  hideMessage("spanHinhSP");
  hideMessage("spanMoTa");
  displayBtn2.add("d-none");
  displayBtn1.remove("d-none");
};