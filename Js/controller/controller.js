// Render sản phẩm
const viTriDanhSachSP = document.getElementById("listProduct");
var iconProduct;
function renderProduct(products) {
  var contentHTML = products
    .map(function (item) {
      if (item.type == "Iphone") {
        iconProduct = `<i class="fab fa-apple"></i>`;
      } else if (item.type == "Samsung") {
        iconProduct = `Samsung`;
      }
      return `
            <div id="${item.id}" class="product__item" type="${item.type}">
              <div class="product__item-top">
                <div class="item-img">
                  <div class="img-top">
                    ${iconProduct}
                    <p>In Stock</p>
                  </div>
                  <img
                    src="${item.img}"
                    alt=""
                  />
                </div>
              </div>
              <div class="product__item-bottom">
                <div class="item-name">${item.name}</div>
                <div class="item-price">Giá: <span>$${item.price}</span></div>
                <div class="item-backcam">
                  Camera sau: <span>${item.backCamera}</span>
                </div>
                <div class="item-frontcam">Camera trước: <span>${item.frontCamera}</span></div>
                <div class="item-desc">${item.desc}</div>
                <span class="btn-add"><span onclick="addToCart(${item.id}, this)">Thêm >></span></span>
              </div>
            </div>
        `;
    })
    .join("");
  viTriDanhSachSP.innerHTML = contentHTML;
}

function changeAddButton(idChange, element, cartChange) {
  cartChange.map((item) => {
    if (item.product.id !== idChange) {
      element.innerHTML = `
              <div class="qty-change">
                <button class="btn-qty" onclick="qtyChangeItem(this, 'sub')">
                  <i class="fas fa-chevron-left"></i>
                </button>
                <p class="qty">${item.quantity}</p>
                <button class="btn-qty" onclick="qtyChangeItem(this, 'add')">
                  <i class="fas fa-chevron-right"></i>
                </button>
              </div>
      `;
    }
  });
  changeItem();
}

// Render giao diện lần đầu

function changeItem() {
  cart.map((item) => {
    if (document.getElementById(`${item.product.id}`) !== null) {
      document
        .getElementById(`${item.product.id}`)
        .querySelector(".btn-add").innerHTML = `
    <div class="qty-change">
       <button class="btn-qty" onclick="qtyChangeItem(this, 'sub')">
         <i class="fas fa-chevron-left"></i>
       </button>
       <p class="qty">${item.quantity}</p>
       <button class="btn-qty" onclick="qtyChangeItem(this, 'add')">
         <i class="fas fa-chevron-right"></i>
       </button>
    </div>
`;
    }
  });
  
}


function qtyChangeItem(element, method) {

  var spanEl = element.parentNode.parentNode;
 
  var productItem = spanEl.parentNode.parentNode;
  if (method == "add") {
    document
      .getElementById(`C${productItem.id}`)
      .querySelector(".btn-qty.add")
      .click();
      changeItem()
  } else if (method == "sub") {
    if (spanEl.querySelector(".qty").innerHTML == 1) {
      document
        .getElementById(`C${productItem.id}`)
        .querySelector(".remove")
        .click();
      productItem.querySelector(".product__item-bottom .btn-add").innerHTML = `
      <span onclick="addToCart(${item.id}, this)">Thêm >></span>
      `
    } else {
      document
        .getElementById(`C${productItem.id}`)
        .querySelector(".btn-qty.sub")
        .click();
        changeItem()
    }
  }
}

// Render Cart
function renderInCart(data) {
  var contentCartHTML = data
    .map(function (item) {
      return `
            <div id="C${item.product.id}" class="cart-item">
              <div class="cart-img">
                <img
                  src="${item.product.linkimg}"
                  alt=""
                />
              </div>
              <strong class="name">${item.product.name}</strong>
              <div class="qty-change">
                <button class="btn-qty sub" onclick="qtyChange(this,'sub')">
                  <i class="fas fa-chevron-left"></i>
                </button>
                <p class="qty">${item.quantity}</p>
                <button class="btn-qty add" onclick="qtyChange(this,'add')">
                  <i class="fas fa-chevron-right"></i>
                </button>
              </div>
              <p class="price">${item.product.price} $</p>
              <button class="remove" onclick="removeItem(this)">
                <i class="fas fa-trash"></i>
              </button>
            </div>
  `;
    })
    .join("");
  document.querySelector(".cart-items").innerHTML = contentCartHTML;
}

// Render tong gia tien

var finalTotal = document.querySelector(".final strong .total");
var totalQty = document.querySelector(".shopping .total-qty");
function renderTotal(totalCart) {
  var totalCost = 0;
  totalCart.map((item) => {
    totalCost += parseInt(item.product.price);
  });
  finalTotal.innerHTML = totalCost;
}

// Render tong san pham trong gio hang

function renderTotalQty(totalCart) {
  var totalQantity = 0;
  totalCart.map((item) => {
    totalQantity += parseInt(item.quantity);
  });
  totalQty.innerHTML = totalQantity;
}

// Render Payment

function renderPayment(paymentCart) {
  document.querySelector(".invoice .shipping-items").innerHTML = paymentCart
    .map((item) => {
      return `
          <div class="shipping-item">
            <span class="item-name">${item.quantity} x ${item.product.name}</span>
            <span class="item-price">${item.product.price} $</span>
          </div>
    `;
    })
    .join("");
  document.querySelector(".total-pay .pay").innerHTML =
    finalTotal.innerHTML + " $";
}
