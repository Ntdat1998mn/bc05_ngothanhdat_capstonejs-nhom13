const BASE_URL = "https://63778daf5c47776512211558.mockapi.io";
let error = "Hệ thống đang sử lí. Vui lòng thực hiện lại sau!";
var cart = [];

// Luu localstorage

const CART = "CART";
function saveLocalStorage() {
  let jsonCart = JSON.stringify(cart);
  localStorage.setItem(CART, jsonCart);
}
// Goi localStorage
var dataJson = localStorage.getItem(CART);
if (dataJson !== null) {
  var cartArray = JSON.parse(dataJson);
  cartArray.map((item) => {
    var item = new cartItem(
      item.product.id,
      item.product.linkimg,
      item.product.price,
      item.product.name,
      item.quantity
    );
    cart.push(item);
  });
  renderInCart(cart);
  renderTotalQty(cart);
  renderTotal(cart);
}

// Goi Render lan dau
function fetchAllProduct() {
  axios({
    url: `${BASE_URL}/Product`,
    method: "GET",
  })
    .then(function (res) {
      renderProduct(createNewDataFollowType(res.data));
      changeItem();
    })
    .catch(function () {
      alert(error);
    });
}

fetchAllProduct();
// Tao new Data có theo từng loại type trong thẻ select
function createNewDataFollowType(data) {
  var newData = data.filter(function (item) {
    if (theSelectDom.value == "All") {
      newData = data;
      return newData;
    } else {
      return item.type == theSelectDom.value;
    }
  });
  return newData;
}

// Render khi loc san pham
var theSelectDom = document.getElementById("locSP");
theSelectDom.onchange = function () {
  axios({
    url: `${BASE_URL}/Product`,
    method: "GET",
  })
    .then(function (res) {
      renderProduct(createNewDataFollowType(res.data));
      changeItem();
    })
    .catch(function () {
      alert(error);
    });
};

// Them vao gio hang

function addToCart(idAdded, element) {
  axios({
    url: `${BASE_URL}/Product/${idAdded}`,
    method: "GET",
  })
    .then(function (res) {
      var check = true;
      for (var i = 0; i < cart.length; i++) {
        if (idAdded == cart[i].product.id) {
          check = false;
        }
      }
      if (check) {
        var item = new cartItem(
          res.data.id,
          res.data.img,
          res.data.price,
          res.data.name,
          1
        );
        cart.push(item);
      }
      renderInCart(cart);
      renderTotal(cart);
      renderTotalQty(cart);
      saveLocalStorage();
      changeAddButton(idAdded, element, cart);
    })
    .catch(function () {
      alert(error);
    });
}

// Xem gio hang

function xemGioHang() {
  document.querySelector(".shopping__overlay").classList.add("active");
}

// Thoat khoi gio hang
function closed() {
  document.querySelector(".shopping__overlay").classList.remove("active");
}

// Xoa khoi gio hang

function removeItem(element) {
  var parentNodeEl = element.parentNode;
  parentNodeEl.remove();
  cart = cart.filter((item) => {
    return item.product.id !== parentNodeEl.id.substr(1, parentNodeEl.length);
  });
  renderInCart(cart);
  renderTotal(cart);
  renderTotalQty(cart);
  fetchAllProduct();
  saveLocalStorage();
}

// Dieu chinh so luong hang trong gio, hang trong gio khong qua 10 chiec/loai

function qtyChange(element, method) {
  var parentNodeEl = element.parentNode;
  if (method == "add") {
    if (parentNodeEl.querySelector("p.qty").innerHTML < 10) {
      cart.map((item) => {
        if (
          item.product.id ==
          parentNodeEl.parentNode.id.substr(1, parentNodeEl.length)
        ) {
          item.quantity++;
          item.product.price =
            (item.product.price / (item.quantity - 1)) * item.quantity;
        }
      });
      renderInCart(cart);
      renderTotal(cart);
      renderTotalQty(cart);
      saveLocalStorage();
    } else {
      alert(
        "Quý khách chỉ có thể mua tối đa 10 sản phẩm trên mỗi loại mặt hàng."
      );
    }
  } else if (
    method == "sub" &&
    parentNodeEl.querySelector("p.qty").innerHTML > 1
  ) {
    cart.map((item) => {
      if (
        item.product.id ==
        parentNodeEl.parentNode.id.substr(1, parentNodeEl.length)
      ) {
        item.quantity--;
        item.product.price =
          (item.product.price / (item.quantity + 1)) * item.quantity;
      }
    });
    renderInCart(cart);
    renderTotal(cart);
    renderTotalQty(cart);
    saveLocalStorage();
  } else if (
    method == "sub" &&
    parentNodeEl.querySelector("p.qty").innerHTML == 1
  ) {
    removeItem(parentNodeEl);
    fetchAllProduct();
  }
}

// Clear card
function clearCart() {
  cart = [];
  renderInCart(cart);
  renderTotal(cart);
  renderTotalQty(cart);
  fetchAllProduct();
  saveLocalStorage();
}

// Mua san pham
function buy() {
  if (cart.length !== 0) {
    document.querySelector(".invoice").style.display = "block";
    document.querySelector(".shopping__container").style.display = "none";
    document.querySelector(".noticed").style.display = "none";
    renderPayment(cart);
  }
}

// Xac nhan order now
function order() {
  document.querySelector(".invoice").style.display = "none";
  document.querySelector(".shopping__container").style.display = "none";
  document.querySelector(".noticed").style.display = "block";
}

// Cancel
function cancel() {
  document.querySelector(".invoice").style.display = "none";
  document.querySelector(".shopping__container").style.display = "block";
  document.querySelector(".noticed").style.display = "none";
}

// Xac nhan thong bao

function confirmNoticed() {
  document.querySelector(".invoice").style.display = "none";
  document.querySelector(".shopping__container").style.display = "block";
  document.querySelector(".noticed").style.display = "none";
  closed();
  clearCart();
}

// Dieu chinh so luong o tai nut add
